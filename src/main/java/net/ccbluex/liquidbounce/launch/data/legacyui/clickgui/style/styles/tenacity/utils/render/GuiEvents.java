package net.ccbluex.liquidbounce.launch.data.legacyui.clickgui.style.styles.tenacity.utils.render;

public enum GuiEvents {

    DRAW,
    CLICK,
    RELEASE

}
